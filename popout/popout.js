function formPopulate(){
  // Populate the form fields with the current config setting
  
  browser.storage.local.get("preference").then(data => {
    console.log('form populate');
    console.log(data.preference);
    for (var key in data.preference){
      console.log(key);
      let element = document.getElementById(key);
      console.log(element);
      let type = element.type;
      switch(type){
        case "checkbox":
          element.checked = data.preference[key];
          continue;
        default:
          element.value = data.preference[key];
          continue;
      }
    }
  });
}

function formSubmit(form){
  console.log(form);
  var data = {};
  for(var i=0; i<form.target.length; i++){
    let element = form.target[i]
    let type = element.type
    console.log(element);
    switch(type){
      case "checkbox":
        data[element.id] = element.checked || false;
        continue;
      case "number":
        data[element.id] = element.valueAsNumber || null;
        continue;
      case "select-one":
        data[element.id] = element.value || null;
        continue;
      case "textarea":
        data[element.id] = element.value || null;
        continue;
      default:
        continue;
    }
  }
  console.log(data)
  browser.storage.local.set({"preference": data}).then( err => {
    formPopulate();
  });
}

//Prefs form populate with current prefs
formPopulate();

//Bring in the background scripts
const bgjs = browser.extension.getBackgroundPage()

//Markup Bindings
document.getElementById('preferenceForm').addEventListener("submit", formSubmit);
document.getElementById('actionSort').addEventListener("click", bgjs.tabSort);
document.getElementById('actionBankruptcy').addEventListener("click", bgjs.tabBankruptcy);
