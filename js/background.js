const defaultPrefs = {
    "ignorePinned": true,
    "ignoreDomains": null,
    "selectedOnly": false,
    "contSortEnabled": false,
    "contSortType": "access",
    "contCleanupTime": 120,
    "actionBankruptTime": 10,
    "actionSortType": "access"
  }

// ===========
// Tab Actions
// ===========
function runTabRules(callbackSuccess, callbackError){
  console.log("Running tab rules");
  withPreferredTabs((prefs, tabs) => {
    tabs.forEach(tab => {
      //Execute the auto-clean
      if(((new Date).getTime() - tab.lastAccessed) > prefs.constCleanupTime*60000 ){
        browser.tabs.remove(tab.id);
      }
    });
    callbackSuccess();
  }, err => {
    callbackError(err);
  });
}

function tabBankruptcy(callbackSuccess, callbackError){
  console.log("Running tab bankruptcy");
  withPreferredTabs((prefs, tabs) => {
    tabs.forEach(tab => {
      //Execute the auto-clean
      if(((new Date).getTime() - tab.lastAccessed) > prefs.actionBankruptTime*60000 ){
        browser.tabs.remove(tab.id);
      }
    });
  }, err => {
    callbackError(err);
  });
}

function tabSort(callbackSuccess, callbackError){
  console.log("Running tab sort");
  withPreferredTabs((prefs, tabs) => {
    tabs.forEach(tab => {
      // Not implemented
    });
  }, err => {
    callbackError(err);
  });
}

// =============
// Default prefs
// =============
function checkAndSetDefaultPrefs(callback){
  browser.storage.local.get("preference").then(data => {
    if(data.preference === undefined){
      browser.storage.local.set({"preference": defaultPrefs}).then( err => {
        callback();
      });
    } else {
      callback();
    }
  });
}


// ===============
// Event loop code
// ===============
function delayScheduleRun(err){
  if(err !== undefined){
    console.log("re-delaying, probably because of an error");
  }
  setTimeout(scheduleRun, 120000);
}

function scheduleRun(){
  window.requestIdleCallback(function(){ runTabRules(delayScheduleRun, delayScheduleRun); });
}
console.log('background script started');
console.log('checking default prefs and scheduling first run');
checkAndSetDefaultPrefs(scheduleRun);
