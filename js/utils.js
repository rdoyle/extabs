// I call this part "Why I hate functional programming"
function withPreferredTabs(callbackSuccess, callbackError){
  execWithPrefs(prefs => {
    withSelectedTabs(prefs, tabs => {
      callbackSuccess(prefs, tabs);
    }, err => {
      console.log(`failed opening tabs with error: ${err}`);
      callbackError(err);
    });
  }, err => {
    console.log(`failed storage with error: ${err}`);
    callbackError(err);
  });
}

function execWithPrefs(callbackSuccess, callbackError){
  browser.storage.local.get("preference").then(data => {
    const prefs = data.preference;
    if(prefs === undefined || Object.keys(prefs).length < 3){
      callbackError("Preference fetch failed with too few keys in preference object.  Likely object is not set.");
      return;
    }
    callbackSuccess(prefs);
  }, err => {
    callbackError(err);
  });
}

function withSelectedTabs(prefs, callbackSuccess, callbackError){
  var builtQuery = {};

  if(prefs.ignorePinned){
    builtQuery["pinned"] = false;
  }
  if(prefs.ignoreActive){
    builtQuery["active"] = false;
  }

  browser.tabs.query(builtQuery).then(tabs => {
    callbackSuccess(tabs);
  }, err => {
    callbackError(err);
  });
}


